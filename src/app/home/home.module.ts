import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardComponent } from './board/board.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { AuthenticationService } from '../services/authentication.service';
import { AuthGuardService } from '../services/auth-guard.service';
import { StadiumsComponent } from './stadiums/stadiums.component';
import { AboutComponent } from './about/about.component';
import { StadiumEditComponent } from './stadiums/stadium-edit/stadium-edit.component';
import { AgmCoreModule } from '@agm/core';
import { StadiumDeleteComponent } from './stadiums/stadium-delete/stadium-delete.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [
        ProfileComponent,
        HomeComponent,
        BoardComponent,
        SidebarComponent,
        StadiumsComponent,
        AboutComponent,
        StadiumEditComponent,
        StadiumDeleteComponent
    ],
    imports: [
        AgmCoreModule.forRoot({
            apiKey: "AIzaSyDcMQwojSbOVRiQtgo2zo3kHdPinh2FcZY",
            libraries: ["places", "geometry"]
        }),
        CommonModule,
        HomeRoutingModule,
        NgbModule.forRoot(),
        FormsModule,
        ReactiveFormsModule
    ],
    entryComponents: [
        StadiumDeleteComponent
    ],
    providers: [
        AuthenticationService, 
        AuthGuardService
    ],
    exports: [
        ProfileComponent,
        HomeComponent,
        BoardComponent,
        SidebarComponent
    ],
    bootstrap: [HomeComponent]
})
export class HomeModule { }