import { Component, OnInit } from '@angular/core'
import { TitleService } from '../../services/title.service'

@Component({
	selector: 'app-about',
	templateUrl: './about.component.html',
	styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

	constructor(
		public titleService: TitleService
	) {
	}

	ngOnInit() {
		this.titleService.setHomeTitle("About")
	}

}
