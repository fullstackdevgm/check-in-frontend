import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardComponent } from './board/board.component';
import { StadiumsComponent } from './stadiums/stadiums.component';
import { AboutComponent } from './about/about.component';
import { StadiumEditComponent } from './stadiums/stadium-edit/stadium-edit.component';

import { AuthGuardService } from '../services/auth-guard.service';

const routes: Routes = [
	{
		path: 'home',
		component: HomeComponent,
		canActivate: [AuthGuardService],
		children: [
			{ path: '', component: StadiumsComponent },
			{ path: 'stadium/:id', component: StadiumEditComponent },
			{ path: 'stadium', component: StadiumEditComponent },
			{ path: 'profile', component: ProfileComponent },
			{ path: 'board/:id', component: BoardComponent },
			{ path: 'board', component: BoardComponent },
			{ path: 'about', component: AboutComponent },
		]
	}
];

@NgModule({
	imports: [ RouterModule.forChild(routes) ],
	exports: [ RouterModule ]
})
export class HomeRoutingModule {}