import { Component, OnInit } from '@angular/core'
import { TitleService } from '../services/title.service'


@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	status: boolean = false

	get title():string {
		return this.titleService.getHomeTitle()
	}

	constructor(public titleService: TitleService) { }

	ngOnInit() {
		this.titleService.setHomeTitle("Dashboard")
	}

	toggleSidebar() {
		this.status = !this.status;
	}

}
