import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { UserDetails } from '../../classes/user-details';
import { Subscription, Observable } from 'rxjs'

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {

	@Input() status: boolean;
	user: UserDetails
	profileSubscription: Subscription

	constructor( public auth: AuthenticationService ) {
		this.user = {
			_id: undefined,
			email: "user@user.com",
			name: "User",
			exp: 999,
			iat: 999
		}
	}

	ngOnInit() {
		this.profileSubscription = this.auth.profile().subscribe(user => {
			this.user = user;
		}, (err) => {
			console.error(err);
		});
	}

	ngOnDestroy() {
		this.profileSubscription.unsubscribe()
	}

}
