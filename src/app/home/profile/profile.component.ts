import { Component, OnInit, OnDestroy, ViewRef, ChangeDetectorRef, ViewChild } from '@angular/core'
import { MapsAPILoader } from '@agm/core';
import { Subscription, Observable } from 'rxjs'

import { AuthenticationService } from '../../services/authentication.service'
import { UserDetails } from '../../classes/user-details';
import { TitleService } from '../../services/title.service'
import { LocationService } from '../../services/location.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
	user: UserDetails;
	coordinates: {
		lat: number,
		long: number
	} = {
		lat: 39.8282,
		long: -98.5795
	};
	currentLocation: string;
	zoom: number = 12;
	intervalHolder: any;
	addressSubscription : Subscription
	postionSubscription : Subscription
	profileSubscription : Subscription

	constructor(
		private auth: AuthenticationService,
		private locationService: LocationService,
		private mapsAPILoader: MapsAPILoader,
		private ref: ChangeDetectorRef,
		public titleService: TitleService
		) {
	}

	ngOnInit() {
		this.titleService.setHomeTitle("Profile")
		
		this.profileSubscription = this.auth.profile().subscribe(user => {
			this.user = user;
		}, (err) => {
			console.error(err);
		});

		this.postionSubscription = this.locationService.currentPosition.subscribe(position => {
			this.coordinates.lat = position.lat;
			this.coordinates.long = position.lng;
			this.addressSubscription = this.locationService.getAddress( this.coordinates.lat, this.coordinates.long, true ).subscribe(address => {
				this.currentLocation = address;
				this.intervalHolder = setInterval(() => {
					if( this.ref !== null && this.ref !== undefined && !(this.ref as ViewRef).destroyed ) {
						this.ref.detectChanges();
					}
				}, 100 );
			});
		})
		
	}

	ngOnDestroy() {
		if (this.addressSubscription) this.addressSubscription.unsubscribe();
		this.postionSubscription.unsubscribe();
		this.profileSubscription.unsubscribe();
		clearInterval(this.intervalHolder);
	}
}