/// <reference types="@types/googlemaps" />
import { ElementRef, NgZone, Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { Router, ActivatedRoute } from '@angular/router';

import { StadiumService } from '../../../services/stadium.service';
import { LocationService } from '../../../services/location.service';
import { TitleService } from '../../../services/title.service'
import { Stadium } from '../../../classes/stadium';
import { Subscription, Observable } from 'rxjs'


@Component({
	selector: 'app-stadium-edit',
	templateUrl: './stadium-edit.component.html',
	styleUrls: ['./stadium-edit.component.css']
})
export class StadiumEditComponent implements OnInit, OnDestroy {
	public message: string;
	public stadiumId: string;
	autocomplete: any;

	public stadium: Stadium = {
		_id: "",
		name: "",
		image: "",
		lat: 39.8282,
		long: -98.5795
	};
	public zoom: number = 12;

	@ViewChild("search")
	public searchElementRef: ElementRef;

	stadiumSubscription: Subscription
	postionSubscription: Subscription
	addstadiumSubscription: Subscription
	updatestadiumSubscription: Subscription
	saveImageSubscription: Subscription

	constructor(
		private mapsAPILoader: MapsAPILoader,
		private route: ActivatedRoute,
		private stadiumService: StadiumService,
		private locationService: LocationService,
		private router: Router,
		public titleService: TitleService,
		private ngZone: NgZone
		) { }

	ngOnInit() {
		if (this.route.snapshot.params['id']) {
			this.stadiumId = this.route.snapshot.params['id'];
		}

		if( this.isNew() ) {
			this.titleService.setHomeTitle("New Stadium")
		} else {
			this.titleService.setHomeTitle("Edit Stadium")
		}

		if(!this.isNew()) {
			this.stadiumSubscription = this.stadiumService.getStadium(this.stadiumId).subscribe((stadium) => {
				this.stadium = stadium;
			})
		}

		if( this.isNew() ) {
			this.postionSubscription = this.locationService.currentPosition.subscribe(location => {
				this.stadium.lat = location.lat
				this.stadium.long = location.lng
			});
		}

		this.mapsAPILoader.load().then(() => {
			this.autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
				types: []
			});
			this.autocomplete.addListener("place_changed", () => {
				this.ngZone.run(() => {
					let place: google.maps.places.PlaceResult = this.autocomplete.getPlace();

					if (place.geometry === undefined || place.geometry === null) {
						return;
					}

					this.stadium.lat = place.geometry.location.lat();
					this.stadium.long = place.geometry.location.lng();
					this.zoom = 12;
				});
			});
		});
	}

	ngOnDestroy() {
		if(this.stadiumSubscription) this.stadiumSubscription.unsubscribe()
		if(this.addstadiumSubscription) this.addstadiumSubscription.unsubscribe()
		if(this.updatestadiumSubscription) this.updatestadiumSubscription.unsubscribe()
		if(this.saveImageSubscription) this.saveImageSubscription.unsubscribe()
		if(this.postionSubscription) this.postionSubscription.unsubscribe()
	}

	preview(files) {
		if(files.length === 0) {
			return
		}

		var mimeType = files[0].type;
		if (mimeType.match(/image\/*/) == null) {
			this.message = "Only images are supported.";
			return;
		}

		this.saveImageSubscription = this.stadiumService.saveImage(files[0]).subscribe(filename => {
			this.stadium.image = filename;
		});
	}

	private isNew() {
		if ( this.stadiumId ) {
			return false;
		}
		return true;
	}

	onSubmit() {
		if (this.isNew()) {
			this.addstadiumSubscription = this.stadiumService.addStadium(this.stadium).subscribe((stadium) => {
				this.router.navigateByUrl('/home');
			}, (err) => {
				console.error(err);
			});
		} else {
			this.updatestadiumSubscription = this.stadiumService.updateStadium(this.stadium).subscribe((stadium) => {
				this.router.navigateByUrl('/home');
			}, (err) => {
				console.error(err);
			});
		}
	}
}
