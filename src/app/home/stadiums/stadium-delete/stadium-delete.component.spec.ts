import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StadiumDeleteComponent } from './stadium-delete.component';

describe('StadiumDeleteComponent', () => {
  let component: StadiumDeleteComponent;
  let fixture: ComponentFixture<StadiumDeleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StadiumDeleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StadiumDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
