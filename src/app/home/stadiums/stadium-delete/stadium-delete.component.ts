import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core'
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'

import { Stadium } from '../../../classes/stadium'

@Component({
	selector: 'app-stadium-delete',
	templateUrl: './stadium-delete.component.html',
	styleUrls: ['./stadium-delete.component.css']
})
export class StadiumDeleteComponent implements OnInit {
	@Input() stadium : Stadium
	@Output() deletePressed : EventEmitter<Stadium> = new EventEmitter<Stadium>()

	constructor(
		public activeModal: NgbActiveModal
	) { }

	ngOnInit() {
	}

	cancel() {
        this.activeModal.close()
    }

    delete() {
        this.deletePressed.emit(this.stadium)
        this.activeModal.close()
    }

}
