import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal }  from '@ng-bootstrap/ng-bootstrap'

import { TitleService } from '../../services/title.service'
import { Stadium } from '../../classes/stadium';
import { StadiumService } from '../../services/stadium.service';

import { StadiumDeleteComponent }    from './stadium-delete/stadium-delete.component';
import { Subscription, Observable } from 'rxjs'


@Component({
	selector: 'app-stadiums',
	templateUrl: './stadiums.component.html',
	styleUrls: ['./stadiums.component.css']
})
export class StadiumsComponent implements OnInit, OnDestroy {
	stadiums: Stadium[] = []
	isDeleteDialogOpen : boolean = false

	stadiumsSubscription: Subscription
	deletePressedSubscription: Subscription
	deleteStadiumSubscription: Subscription

	constructor(
		public titleService: TitleService,
		private router: Router,
		private modalService : NgbModal,
		private stadiumService: StadiumService
		) {
	}

	ngOnInit() {
		this.titleService.setHomeTitle("Stadiums List")

		this.stadiumsSubscription = this.stadiumService.getStadiums().subscribe((stadiums) => {
			this.stadiums = stadiums;
		})
	}

	ngOnDestroy() {
		this.stadiumsSubscription.unsubscribe()
		if(this.deletePressedSubscription) this.deletePressedSubscription.unsubscribe()
		if(this.deleteStadiumSubscription) this.deleteStadiumSubscription.unsubscribe()
	}

	navigateUrl(url: string) {
		this.router.navigateByUrl(url);
	}

	private showDeleteConfirmationModal(st: Stadium) {
		const modalRef = this.modalService.open(StadiumDeleteComponent)
		this.isDeleteDialogOpen = true
		const deleteComponent : StadiumDeleteComponent = modalRef.componentInstance

		deleteComponent.stadium = st
		this.deletePressedSubscription = deleteComponent.deletePressed.subscribe(stadium => {
			this.deleteStadiumSubscription = this.stadiumService.deleteStadium(stadium).subscribe(stadium => {
				this.ngOnInit()
			})
		})
		modalRef.result.then(result => {
			this.isDeleteDialogOpen = false
		})
	}

}
