import { Component, OnInit, OnDestroy, ViewRef, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MapsAPILoader } from '@agm/core';
import { Subscription, Observable } from 'rxjs'

import { StadiumService } from '../../services/stadium.service';
import { LocationService } from '../../services/location.service';
import { CheckInService } from '../../services/check-in.service';
import { TitleService } from '../../services/title.service'
import { AuthenticationService } from '../../services/authentication.service'
import { UserDetails } from '../../classes/user-details';
import { Stadium } from '../../classes/stadium';
import { Status } from '../../classes/status';

@Component({
	selector: 'app-board',
	templateUrl: './board.component.html',
	styleUrls: ['./board.component.css']
})
export class BoardComponent implements OnInit, OnDestroy {
	public stadiumId: string;
	public zoom: number = 12;
	public addressValue: string = "";
	user: UserDetails;
	private _percent: number = 0

	public stadium: Stadium = {
		_id: "",
		name: "",
		image: "",
		lat: 39.8282,
		long: -98.5795
	};
	public coordinates: {
		lat: number,
		long: number
	} = {
		lat: 39.8282,
		long: -98.5795
	};
	public status: Status = {
		_id: "",
		userId: "",
		stadiumId: "",
		status: false
	}

	intervalHolder: any;
	addressSubscription : Subscription
	postionSubscription : Subscription
	profileSubscription : Subscription
	stadiumSubscription : Subscription
	stadiumsSubscription : Subscription
	statusSubscription : Subscription
	statusCreateSubscription : Subscription
	statusUpdateSubscription : Subscription
	percentSubscription : Subscription

	constructor(
		private stadiumService: StadiumService,
		private statusService: CheckInService,
		private locationService: LocationService,
		private auth: AuthenticationService,
		private mapsAPILoader: MapsAPILoader,
		private route: ActivatedRoute,
		private ref: ChangeDetectorRef,
		public titleService: TitleService
		) {
	}

	ngOnInit() {
		if (this.route.snapshot.params['id']) {
			this.stadiumId = this.route.snapshot.params['id'];
		}

		if(this.isNew()) {

			this.postionSubscription = this.locationService.currentPosition.subscribe(location => {
				this.coordinates.lat = location.lat
				this.coordinates.long = location.lng
			});

			this.titleService.setHomeTitle( "Nearest Stadium" )

			this.mapsAPILoader.load().then(() => {
				this.stadiumsSubscription = this.stadiumService.getStadiums().subscribe(stadiums => {
					let distance = 0;
					stadiums.forEach(st => {
						const dist = this.locationService.getDistance(this.coordinates, st);
						if (distance == 0) {
							distance = dist
							this.stadium = st
						} else {
							if (distance > dist) {
								distance = dist
								this.stadium = st
							}
						}
					});
					this.status.stadiumId = this.stadium._id

					this.locationService.getAddress( this.stadium.lat, this.stadium.long, false ).subscribe(address => {
						this.addressValue = address
						this.intervalHolder = setInterval(() => {
							if( this.ref !== null && this.ref !== undefined && !(this.ref as ViewRef).destroyed ) {
								this.ref.detectChanges();
							}
						}, 100 );
					});
					this.percentSubscription = this.statusService.getPercent(this.stadium).subscribe(per => {
						this._percent = per;
					})

					this.profileSubscription = this.auth.profile().subscribe(user => {
						this.user = user;
						this.status.userId = this.user._id

						this.statusSubscription = this.statusService.searchStatus(this.status).subscribe(statusList => {
							if(statusList.length > 0) {
								this.status = statusList[0]
							} else {
								this.statusCreateSubscription = this.statusService.addStatus(this.status).subscribe(status => {
									this.status = status
								})
							}
						});
					}, (err) => {
						console.error(err);
					});
				});
			});
		} else {
			this.stadiumSubscription = this.stadiumService.getStadium(this.stadiumId).subscribe(stadium => {
				this.stadium = stadium;
				this.status.stadiumId = this.stadium._id
				this.titleService.setHomeTitle( this.stadium.name )
				this.locationService.getAddress( this.stadium.lat, this.stadium.long, false ).subscribe(address => {
					this.addressValue = address
					this.intervalHolder = setInterval(() => {
						if( this.ref !== null && this.ref !== undefined && !(this.ref as ViewRef).destroyed ) {
							this.ref.detectChanges();
						}
					}, 100 );
				});
				this.percentSubscription = this.statusService.getPercent(this.stadium).subscribe(per => {
					this._percent = per;
				})

				this.profileSubscription = this.auth.profile().subscribe(user => {
					this.user = user;
					this.status.userId = this.user._id

					this.statusSubscription = this.statusService.searchStatus(this.status).subscribe(statusList => {
						if(statusList.length > 0) {
							this.status = statusList[0]
						} else {
							this.statusCreateSubscription = this.statusService.addStatus(this.status).subscribe(status => {
								this.status = status
							})
						}
					});
				}, (err) => {
					console.error(err);
				});
			})
		}
	}

	ngOnDestroy() {
		if (this.addressSubscription) this.addressSubscription.unsubscribe();
		if (this.stadiumSubscription) this.stadiumSubscription.unsubscribe();
		if (this.stadiumsSubscription) this.stadiumsSubscription.unsubscribe();
		if (this.postionSubscription) this.postionSubscription.unsubscribe();
		if (this.percentSubscription) this.percentSubscription.unsubscribe();
		if (this.statusSubscription) this.statusSubscription.unsubscribe();
		if (this.statusCreateSubscription) this.statusCreateSubscription.unsubscribe();
		if (this.statusUpdateSubscription) this.statusUpdateSubscription.unsubscribe();
		this.profileSubscription.unsubscribe();
		clearInterval(this.intervalHolder);
	}

	checkedStadium(e) {
		if(!this.status.status) {
			this.status.status = true
			this.statusUpdateSubscription = this.statusService.updateStatus(this.status).subscribe(status => {
				this.ngOnInit()
			})
		}
	}

	private isNew() {
		if ( this.stadiumId ) {
			return false;
		}
		return true;
	}

	get percent() {
		return this._percent;
	}
}
