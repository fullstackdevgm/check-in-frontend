import { Injectable } from '@angular/core';
import { NotifierService } from 'angular-notifier';

import { BaseService } from './base.service';

@Injectable({
	providedIn: 'root'
})
export class TitleService extends BaseService {
	constructor(
		notifierService: NotifierService
	) {
		super(notifierService)
	}

	setHomeTitle(value: string):void {
		localStorage.setItem('check-in/home-title', value)
	}

	getHomeTitle(): string {
		const title = localStorage.getItem('check-in/home-title')
		return title
	}
}
