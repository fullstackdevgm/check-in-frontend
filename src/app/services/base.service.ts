import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { NotifierService } from 'angular-notifier';

@Injectable({
	providedIn: 'root'
})
export class BaseService {

	constructor(public notifierService: NotifierService) { }

	protected handleError<T> (operation = 'operation', result?: T) {
		return (error: any): Observable<T> => {

			console.error(error);

			this.notifierService.notify('error', error);

			return of(result as T);
		};
	}

	protected handleDefault(type = 'User', message?: string) {
		console.log(message);
		this.notifierService.notify('default', `${type}: ${message}`);
	}

	protected handleWarning(type = 'User', message?: string) {
		console.log(message);
		this.notifierService.notify('warning', `${type}: ${message}`);
	}

	protected handleInfo(type = 'User', message?: string) {
		console.log(message);
		this.notifierService.notify('info', `${type}: ${message}`);
	}

	protected handleSuccess(type = 'User', message?: string) {
		console.log(message);
		this.notifierService.notify('success', `${type}: ${message}`);
	}
}
