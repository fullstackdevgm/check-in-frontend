import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

import { Stadium } from '../classes/stadium';
import { BaseService } from './base.service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class StadiumService extends BaseService {
	private stadiumsUrl: string = `/api/stadiums`
	private type: string = "Stadium"

	constructor(
		private http: HttpClient,
		notifierService: NotifierService
	) {
		super(notifierService)
	}

	getStadiums (): Observable<Stadium[]> {
		return this.http.get<any>(this.stadiumsUrl).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError('getStadiums', []))
			);
	}

	getStadiumNo404<Data>(id: string): Observable<Stadium> {
		const url = `${this.stadiumsUrl}/?id=${id}`;
		return this.http.get<any>(url)
		.pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data[0];
			}),
			catchError(this.handleError<Stadium>(`getStadium id=${id}`))
			);
	}

	getStadium(id: string): Observable<Stadium> {
		const url = `${this.stadiumsUrl}/${id}`;
		return this.http.get<any>(url).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium>(`getStadium id=${id}`))
			);
	}

	searchStadiums(term: string): Observable<Stadium[]> {
		if (!term.trim()) {
			return of([]);
		}
		return this.http.get<any>(`${this.stadiumsUrl}/?name=${term}`).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium[]>('searchStadiums', []))
			);
	}

	addStadium (stadium: Stadium): Observable<Stadium> {
		return this.http.post<any>(this.stadiumsUrl, stadium, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium>('addStadium'))
			);
	}

	deleteStadium (stadium: Stadium | number): Observable<Stadium> {
		const id = typeof stadium === 'number' ? stadium : stadium._id;
		const url = `${this.stadiumsUrl}/${id}`;

		return this.http.delete<any>(url, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium>('deleteStadium'))
			);
	}

	updateStadium (stadium: Stadium): Observable<any> {
		const url = `${this.stadiumsUrl}/${stadium._id}`;
		return this.http.put<any>(url, stadium, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium>('updateStadium'))
			);
	}

	saveImage (selectedFile): Observable<any> {
		let headers = new HttpHeaders();
		headers.set('Content-Type', null);
		headers.set('Accept', "multipart/form-data");
		const uploadData: FormData = new FormData();
		uploadData.append('files', selectedFile, selectedFile.name);
		const url=`/upload/stadium-img`

		return this.http.post<any>(url, uploadData, { headers }).pipe(
			map(data => {
				this.handleSuccess(this.type + 'Image', data.message);
				return data.data;
			}),
			catchError(this.handleError<Stadium>('saveImage'))
			);
	}
}