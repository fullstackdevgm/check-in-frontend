import { Injectable } from '@angular/core';
import { LatLngLiteral, MapsAPILoader } from '@agm/core';
import { Observable, ReplaySubject, Subject } from 'rxjs';
import { NotifierService } from 'angular-notifier';

import { BaseService } from './base.service';


@Injectable({
	providedIn: 'root'
})
export class LocationService extends BaseService {
	private addressValue: ReplaySubject<string>
	private stadiumAddress: ReplaySubject<string>
	private currentLocation: ReplaySubject<LatLngLiteral> = new ReplaySubject<LatLngLiteral>(1)

	constructor(
		private mapsAPILoader: MapsAPILoader,
		notifierService: NotifierService
		) {
		super(notifierService)
		this.addressValue = new ReplaySubject<string>(1)
		this.stadiumAddress = new ReplaySubject<string>(1)
	}

	getAddress(latitude, longitude, key) : Observable<string> {
		let latlng = {lat: latitude, lng: longitude};
		this.mapsAPILoader.load().then(() => {
			let geocoder = new google.maps.Geocoder
			geocoder.geocode({'location': latlng}, (results) => {
				if (results && results[0]) {
					if(key) {
						this.addressValue.next(results[0].formatted_address);
					} else {
						this.stadiumAddress.next(results[0].formatted_address);
					}
				} else {
					this.handleError<any>('No addresses found');
				}
			});
		})

		if(key) {
			return this.addressValue
		}
		return this.stadiumAddress
	}

	getDistance(location, stadium) {
		let distance = 0;
		const lo1 = new google.maps.LatLng(location.lat, location.long);
		const lo2 = new google.maps.LatLng(stadium.lat, stadium.long);
		distance = google.maps.geometry.spherical.computeDistanceBetween(lo1, lo2);
		return distance;
	}

	get currentPosition() : Observable<LatLngLiteral> {
		if (!("geolocation" in navigator)) {
			this.handleError<any>('Cannot get current location!');
		}

		const subject = new Subject<LatLngLiteral>()
		navigator.geolocation.getCurrentPosition((position)=> {
			this.currentLocation.next({ lat : position.coords.latitude, lng : position.coords.longitude })
		},
		err => {
			this.currentLocation.error(err)
		})

		return this.currentLocation        
	}
}
