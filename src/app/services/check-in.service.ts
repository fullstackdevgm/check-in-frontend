import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { NotifierService } from 'angular-notifier';

import { Status } from '../classes/status';
import { Stadium } from '../classes/stadium';
import { BaseService } from './base.service';

const httpOptions = {
	headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
	providedIn: 'root'
})
export class CheckInService extends BaseService {
	private statusUrl: string = `/api/status`
	private type: string = "Status"

	constructor(
		private http: HttpClient,
		notifierService: NotifierService
	) {
		super(notifierService)
	}

	getStatusList (): Observable<Status[]> {
		return this.http.get<any>(this.statusUrl).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError('getStatusList', []))
			);
	}

	getStatusNo404<Data>(id: string): Observable<Status> {
		const url = `${this.statusUrl}/?id=${id}`;
		return this.http.get<any>(url)
		.pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data[0];
			}),
			catchError(this.handleError<Status>(`getStatus id=${id}`))
			);
	}

	getStatus(id: string): Observable<Status> {
		const url = `${this.statusUrl}/${id}`;
		return this.http.get<any>(url).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status>(`getStatus id=${id}`))
			);
	}

	searchStatus(term: Status): Observable<Status[]> {
		let body = {
			params: {
				'userId': term.userId,
				'stadiumId': term.stadiumId
			}
		}

		return this.http.get<any>(this.statusUrl, body).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status[]>('searchStatus', []))
			);
	}

	addStatus (status: Status): Observable<Status> {
		return this.http.post<any>(this.statusUrl, status, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status>('addStatus'))
			);
	}

	deleteStatus (status: Status | number): Observable<Status> {
		const id = typeof status === 'number' ? status : status._id;
		const url = `${this.statusUrl}/${id}`;

		return this.http.delete<any>(url, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status>('deleteStatus'))
			);
	}

	updateStatus (status: Status): Observable<any> {
		const url = `${this.statusUrl}/${status._id}`;
		return this.http.put<any>(url, status, httpOptions).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status>('updateStatus'))
			);
	}

	getPercent (stadium: Stadium): Observable<any> {
		let body = {
			params: {
				'stadiumId': stadium._id
			}
		}

		const url = `/api/percent`;
		return this.http.get<any>(url, body).pipe(
			map(data => {
				this.handleSuccess(this.type, data.message);
				return data.data;
			}),
			catchError(this.handleError<Status>(`getPercent`))
			);
	}
}
