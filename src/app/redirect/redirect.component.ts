import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
	selector: 'app-redirect',
	templateUrl: './redirect.component.html',
	styleUrls: ['./redirect.component.css']
})
export class RedirectComponent implements OnInit {

	private queryParams : {[key : string] : string} = {}

	constructor(private readonly route : ActivatedRoute, private readonly router : Router) { }

	ngOnInit() {
        this.router.navigateByUrl('/home')
	}

}
