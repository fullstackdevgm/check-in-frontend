export interface Status {
	_id: string;
	userId: string;
	stadiumId: string;
	status: boolean;
}