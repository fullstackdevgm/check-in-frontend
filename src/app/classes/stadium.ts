export interface Stadium {
	_id: string;
	name: string;
	image: string | ArrayBuffer;
	lat: number;
	long: number;
}