import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { TokenPayload } from '../classes/user-details';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs'

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

	credentials: TokenPayload = {
		email: '',
		name: '',
		password: ''
	};

	registerSubscription: Subscription

	constructor( private auth: AuthenticationService, private router: Router ) { }

	ngOnInit() {
	}

	ngOnDestroy() {
		if (this.registerSubscription) this.registerSubscription.unsubscribe()
	}

	register() {
		this.registerSubscription = this.auth.register(this.credentials).subscribe(() => {
			this.router.navigateByUrl('/home/profile');
		}, (err) => {
			console.error(err);
		});
	}

}
