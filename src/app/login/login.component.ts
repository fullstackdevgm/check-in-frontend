import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription, Observable } from 'rxjs'

import { AuthenticationService } from '../services/authentication.service';
import { TokenPayload } from '../classes/user-details';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {
	credentials: TokenPayload = {
		email: '',
		password: ''
	};

	loginSubscription: Subscription

	constructor(
		private auth: AuthenticationService,
		private router: Router
	) {
	}

	ngOnInit() {
	}

	ngOnDestroy() {
		if (this.loginSubscription) this.loginSubscription.unsubscribe()
	}

	login() {
		this.loginSubscription = this.auth.login(this.credentials).subscribe(() => {
			this.router.navigateByUrl('/');
		}, (err) => {
			console.error(err);
		});
	}

}
