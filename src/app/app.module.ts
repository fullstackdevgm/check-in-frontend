import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { NotifierModule } from 'angular-notifier';

import { AppRoutingModule  }     from './app-routing.module';
import { HomeModule } from './home/home.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthenticationService } from './services/authentication.service';
import { AuthGuardService } from './services/auth-guard.service';
import { RedirectComponent } from './redirect/redirect.component';
import { NotImplementedComponent } from './not-implemented/not-implemented.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        RegisterComponent,
        RedirectComponent,
        NotImplementedComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        NotifierModule.withConfig({
            // Custom options in here
        }),
        HomeModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [
        AuthenticationService, 
        AuthGuardService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
